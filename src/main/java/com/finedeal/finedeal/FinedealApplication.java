package com.finedeal.finedeal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinedealApplication {
	public static void main(String[] args) {
		SpringApplication.run(FinedealApplication.class, args);
	}
}
