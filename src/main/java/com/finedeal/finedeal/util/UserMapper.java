package com.finedeal.finedeal.util;

import com.finedeal.finedeal.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new User(
            rs.getInt("id"),
            rs.getString("clientname"),
            rs.getString("clientemail"),
            rs.getString("clientpassword"),
            rs.getString("clientabout"),
            rs.getString("clientconstractorrate"),
            rs.getString("clientcustomerrate")
        );
    }
}
