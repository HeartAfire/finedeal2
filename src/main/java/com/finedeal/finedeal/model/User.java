package com.finedeal.finedeal.model;

public class User {
    private int id;
    private String name;
    private String email;
    private String password;
    private String about;
    private String contractorRate;
    private String customerRate;

    public User(int id, String name, String email, String password, String about, String contractorRate, String customerRate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.about = about;
        this.contractorRate = contractorRate;
        this.customerRate = customerRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getContractorRate() {
        return contractorRate;
    }

    public void setContractorRate(String contractorRate) {
        this.contractorRate = contractorRate;
    }

    public String getCustomerRate() {
        return customerRate;
    }

    public void setCustomerRate(String customerRate) {
        this.customerRate = customerRate;
    }
}
