import { Component } from '@angular/core';
import { ModalService } from './services/modal.service';
import { ModalType } from './enums/modal-type.enum';
import { User, UserDto } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user: User;

  constructor(private modalService: ModalService) {
    this.user = new User({name: 'Test User'} as UserDto);
  }

  onLoginClick() {
    this.modalService.open(ModalType.LogIn);
  }

  onSiginClick() {
    this.modalService.open(ModalType.SignIn);
  }
}
