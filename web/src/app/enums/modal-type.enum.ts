export enum ModalType {
  LogIn,
  SignIn,
  UserInfo,
  DealCreate
}
