import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GreetingPageComponent } from './components/greeting-page/greeting-page.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { UserAccountPageComponent } from './components/user-account-page/user-account-page.component';


const routes: Routes = [
  { path: '', component: GreetingPageComponent },
  { path: 'main', component: MainPageComponent },
  { path: 'user-account', component: UserAccountPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
