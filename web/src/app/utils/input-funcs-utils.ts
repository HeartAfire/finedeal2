export class InputFuncsUtils  {
  public static matchItems(input: string, items: any[]) {
    const trimmedInput = input.trim();
    return items.filter(item => item.toString().toUpperCase().includes(trimmedInput.toUpperCase()));
  }
}
