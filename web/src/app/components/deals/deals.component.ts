import { Component, OnInit } from '@angular/core';
import { Deal, DealDto } from 'src/app/models/deal';
import { FormControl } from '@angular/forms';
import { interval } from 'rxjs';
import { debounce, distinctUntilChanged } from 'rxjs/operators';
import { InputFuncsUtils } from 'src/app/utils/input-funcs-utils';
import * as L from 'leaflet';
import { MapSerivce } from 'src/app/services/map.service';
import { faThumbsDown } from '@fortawesome/free-regular-svg-icons';
import { UserDto, User } from 'src/app/models/user';
import { ModalService } from 'src/app/services/modal.service';
import { ModalType } from 'src/app/enums/modal-type.enum';

const mockOneDto: DealDto = {
  id: 1,
  category: 'Репетиторство',
  desc: 'Отреметируйте пожалуйста',
  cost: '1400р',
  status: 'готов',
  contractor: new User({name: 'Test User2'} as UserDto),
  customer: new User({name: 'Test User'} as UserDto),
  latLng: {lat: 55.74528835536876, lng: 52.45711590029726}
};

const mockTwoDto: DealDto = {
  id: 2,
  category: 'Проституция',
  desc: 'Ну вы поняли',
  cost: '1000р',
  status: 'готов',
  contractor: new User({name: 'Test User2'} as UserDto),
  customer: new User({name: 'Test User'} as UserDto),
  latLng: {lat: 55.725582631776305, lng: 52.469129892102666}
};

const mockDeals = [
  new Deal(mockOneDto),
  new Deal(mockTwoDto)
];

@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.css']
})
export class DealsComponent implements OnInit {
  private dealsLayer: L.FeatureGroup;

  isBrowsingView = true;

  deals = mockDeals;
  matchedDeals: Deal[];
  focusedDeal: Deal;

  dealFormControl = new FormControl('');
  isDealInputFocused = false;

  constructor(private mapService: MapSerivce,
              private modalService: ModalService) {
  }

  ngOnInit() {
    this.initLayer();
    this.matchedDeals = this.deals;
    this.dealFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      debounce(() => interval(500))
    ).subscribe(input => {
      this.matchedDeals = null;
      if (input) this.matchedDeals = InputFuncsUtils.matchItems(input, this.deals);
      else this.matchedDeals = this.deals;
      this.updateLayer();
    });
    this.mapService.mapClicked$.subscribe(() => {
      if (this.isBrowsingView) return;
      console.log('+++');
      
      this.modalService.open(ModalType.DealCreate);
    });
  }

  private initLayer() {
    this.dealsLayer = L.featureGroup(this.deals.map(deal => deal.marker));
    this.mapService.map.addLayer(this.dealsLayer);
  }

  private updateLayer() {
    this.dealsLayer.clearLayers();
    this.dealsLayer = L.featureGroup(this.matchedDeals.map(deal => deal.marker));
    this.mapService.map.addLayer(this.dealsLayer);
  }

  onInputListItemClick(deal: Deal) {
    this.dealFormControl.setValue(deal.toString());
    this.isDealInputFocused = false;
  }

  onDealsInputFocus() {
    this.isDealInputFocused = true;
  }

  onDealsInputBlur() {
    this.isDealInputFocused = false;
  }

  onDealListItemClick(deal: Deal) {
    this.focusOnDeal(deal);
  }

  private focusOnDeal(deal: Deal) {
    this.focusedDeal = deal;
    this.centrateToDeal(deal);
  }

  private centrateToDeal(deal: Deal) {
    this.mapService.map.setView(L.latLng(deal.latLng), this.mapService.map.getZoom());
    setTimeout(() => deal.marker.openPopup(), 300);
  }

  onDealCustomerClick(deal: Deal) {
    this.modalService.open(ModalType.UserInfo, deal.customer);
  }

  onApplyClick() {
    
  }

  onChangeViewClick() {
    this.isBrowsingView = false;
  }

  isInputFocused() {
    return this.matchedDeals && this.matchedDeals.length > 0;
  }
}
