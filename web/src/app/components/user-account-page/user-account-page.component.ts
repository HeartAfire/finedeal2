import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserAccountView } from 'src/app/enums/user-account-view.enum';

@Component({
  selector: 'app-user-account-page',
  templateUrl: './user-account-page.component.html',
  styleUrls: ['./user-account-page.component.css']
})
export class UserAccountPageComponent implements OnInit {
  private currentView = UserAccountView.Main;

  user: User;

  constructor() {
  }

  ngOnInit() {
  }

  getPastDeals() {
    const isPastDealsExist = this.user.deals && this.user.deals.some(deal => deal.status === 'Готов' || deal.status === 'Отменен');
    return isPastDealsExist ? this.user.deals.filter(deal => deal.status === 'Готов' || deal.status === 'Отменен') : null;
  }

  setMainView() {
    this.currentView = UserAccountView.Main;
  }

  isMainView() {
    return this.currentView === UserAccountView.Main;
  }

  setDealsView() {
    this.currentView = UserAccountView.Deals;
  }

  isDealsView() {
    return this.currentView === UserAccountView.Deals;
  }

  setChangePassView() {
    this.currentView = UserAccountView.PasswordChange;
  }

  isChangePassView() {
    return this.currentView === UserAccountView.PasswordChange;
  }

}
