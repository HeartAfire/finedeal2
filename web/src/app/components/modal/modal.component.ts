import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { ModalType } from 'src/app/enums/modal-type.enum';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  modalType: ModalType;
  modalContent: any;

  userLogin: string;
  userPassword: string;
  secondPassword: string;

  dealCategory: string;
  dealDesc: string;
  dealCost: string;

  constructor(private modalService: ModalService) {
    $('#userModal').modal({backdrop: 'static', keyboard: true});
  }

  ngOnInit() {
    this.modalService.openModal$.subscribe(modal => {
      if (modal && (modal.type === ModalType.LogIn || modal.type === ModalType.SignIn)) {
        this.modalType = modal.type;
        $('#modal').modal({backdrop: 'static', keyboard: true});
        this.initModal();
      } else if (modal && modal.type === ModalType.UserInfo) {
        this.modalType = modal.type;
        this.modalContent = modal.content;
        $('#userModal').modal({backdrop: 'static', keyboard: true});
      } else if(modal && modal.type === ModalType.DealCreate) {
        this.modalType = modal.type;
        $('#dealModal').modal({backdrop: 'static', keyboard: true});
        this.initModal();
      }
    });
  }

  private initModal() {
    this.userLogin = '';
    this.userPassword = '';
    this.secondPassword = '';
    this.dealCategory = '';
    this.dealDesc = '';
    this.dealCost = '';
  }

  canSubmit() {
    if (this.modalType === ModalType.LogIn || this.modalType === ModalType.SignIn) {
      const isSignin = this.isSigninModal();
      const isCredintailsExist = this.userLogin && this.userPassword;
      if (isSignin) return isCredintailsExist && this.secondPassword;
      return isCredintailsExist;
    } else if (this.modalType === ModalType.DealCreate) {
      return this.dealCategory && this.dealCost && this.dealDesc;
    }
  }

  onSubmit() {
    const response = this.modalType === ModalType.DealCreate ? {category: this.dealCategory, desc: this.dealDesc, cost: this.dealCost} :
        {login: this.userLogin, password: this.userPassword};
    this.modalService.submit(response);
  }

  onCloseUserInfoClick() {
    $('#userModal').modal({backdrop: 'static', keyboard: true}).modal('hide');
  }

  isLoginModal() {
    return this.modalType === ModalType.LogIn;
  }

  isSigninModal() {
    return this.modalType === ModalType.SignIn;
  }

  isUserInfoModal() {
    return this.modalType === ModalType.UserInfo;
  }

  isDealCreationModal() {
    return this.modalType === ModalType.DealCreate;
  }

  isPasswordsEqual() {
    return !this.secondPassword || this.secondPassword === this.userPassword;
  }
}
