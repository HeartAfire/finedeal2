import { Component, OnInit } from '@angular/core';
import { faStar, faHandPeace, faClock, faThumbsUp } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-main-page',
  templateUrl: './greeting-page.component.html',
  styleUrls: ['./greeting-page.component.css']
})
export class GreetingPageComponent implements OnInit {
  mainPageDesc = "Ищите услугу опытных мастеров или выполнить несложную работу?" +
                 " Множество соискателей, готовых помочь вам, ждут ваших предложений.";
  starIcon = faStar;
  peaceIcon = faHandPeace;
  clockIcon = faClock;
  thumbsUpIcon = faThumbsUp;

  constructor() { }

  ngOnInit() {
  }

}
