import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { MapSerivce } from 'src/app/services/map.service';
import { FinedealService } from 'src/app/services/finedeal.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  constructor(private mapService: MapSerivce, private finedealService: FinedealService) { }

  ngOnInit() {
    this.finedealService.loadUsers();
    this.mapService.initMap();
    this.mapService.mapClicked$.subscribe(clickEvent => {
      if (clickEvent) {
        console.log(clickEvent.latlng);

      }
    });
  }
}
