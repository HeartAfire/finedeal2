import * as L from 'leaflet';
import { User } from './user';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

const icon = L.icon({
  iconUrl: '../../assets/img/marker-icon.png',
  iconSize: [30, 30],
  className: 'marker-icon'
});

const markerStyles = {
  cursor: 'pointer'
};

const markerOptions: L.MarkerOptions = {
  icon
};

export interface DealDto {
  id: number;
  category: string;
  desc: string;
  cost: string;
  customer: User;
  status: string;
  contractor: User;
  latLng: L.LatLngExpression;
}

export class Deal {
  id: number;
  category: string;
  desc: string;
  cost: string;
  customer: User;
  status: string;
  contractor: User;
  latLng: L.LatLngExpression;
  marker: L.Marker;

  constructor(dto: DealDto) {
    this.id = dto.id;
    this.category = dto.category;
    this.desc = dto.desc;
    this.cost = dto.cost;
    this.customer = dto.customer;
    this.status = dto.status;
    this.contractor = dto.contractor;
    this.latLng = dto.latLng;
    this.createMarker();
  }

  private createMarker() {
    const marker = L.marker(this.latLng, markerOptions);
    const popupContent = `<p>${this.desc}</p><p>Заказчик: ${this.customer.name}</p>`;
    marker.bindPopup(popupContent);
    this.marker = marker;
  }

  public getCuttedDesc() {
    return this.desc.length > 25 ? `${this.desc.substring(0, 16)}...` : this.desc;
  }

  public toString() {
    return this.category;
  }
}
