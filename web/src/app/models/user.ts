import { Deal } from './deal';

export interface UserDto {
  id: number;
  name: string;
  email: string;
  password: string;
  about: string;
  contractorRate: number;
  customerRate: number;
}

export class User {
  id: number;
  name: string;
  email: string;
  password: string;
  about: string;
  deals: Deal[];
  contractorRate: number;
  customerRate: number;

  constructor(dto: UserDto) {
    this.id = dto.id;
    this.name = dto.name;
    this.email = dto.email;
    this.password = dto.password;
    this.about = dto.about;
    this.contractorRate = dto.contractorRate;
    this.customerRate = dto.customerRate;
  }
}
