import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const apiUrl = 'http://localhost:8080/api';

@Injectable({providedIn: 'root'})
export class FinedealService {
  constructor(private http: HttpClient) {
  }

  loadUsers() {
    console.log('+++');
    
    this.http.get(`${apiUrl}/users`, {responseType: 'text'}).subscribe(s => console.log(s));
  }
}
