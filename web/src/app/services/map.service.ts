import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, BehaviorSubject } from 'rxjs';
import * as L from 'leaflet';

@Injectable({ providedIn: 'root' })
export class MapSerivce {
  private mapOpened = new ReplaySubject<boolean>(1);
  private mapClicked = new BehaviorSubject<L.MouseEvent>(null);

  public map: L.Map;

  public mapOpened$: Observable<boolean>;
  public mapClicked$: Observable<L.MouseEvent>;

  constructor() {
    this.mapOpened$ = this.mapOpened.asObservable();
    this.mapClicked$ = this.mapClicked.asObservable();
  }

  public initMap() {
    this.map = L.map('map', {minZoom: 13, maxZoom: 18, zoomControl: false}).setView([55.72545, 52.41122], 13);
    this.map.doubleClickZoom.disable();
    this.map.on('click', (event: L.MouseEvent) => this.clickOnMap(event));

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    tiles.addTo(this.map);
  }

  clickOnMap(event: L.MouseEvent) {
    this.mapClicked.next(event);
  }
}
