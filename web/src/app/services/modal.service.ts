import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { config } from 'process';
import { HttpClient } from '@angular/common/http';
import { ModalType } from '../enums/modal-type.enum';

@Injectable({ providedIn: 'root' })
export class ModalService {
  private openModal = new BehaviorSubject(null);
  private submited = new BehaviorSubject(null);

  public openModal$: Observable<any>;
  public submitted$: Observable<any>;

  constructor() {
    this.openModal$ = this.openModal.asObservable();
    this.submitted$ = this.submited.asObservable();
  }

  public open(type: ModalType, content?: any) {
    this.openModal.next({type, content});
  }

  public submit(response: any) {
    this.submited.next(response);
  }
}
